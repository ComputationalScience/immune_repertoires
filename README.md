# Immune Repertoires

Diverse T and B cell repertoires play an important role in mounting effective immune responses against a wide range of pathogens and malignant cells. The number of unique T and B cell clones is characterized by T and B cell receptors (TCRs and BCRs), respectively. Although receptor sequences are generated probabilistically by recombination processes, clinical studies found a high degree of sharing of TCRs and BCRs among different individuals. This repository provides implementations of a mathematical and statistical framework that we formulated in the work "Mathematical Characterization of Private and Public Immune Repertoire Sequences" to quantify receptor distributions.

<div align="center">
<img width="600" src="overlap_sampling_numba.svg" alt="empirical sampling results">
</div>

## Reference
* L. Böttcher, S. Wald, T. Chou, Mathematical Characterization of Private and Public Immune Repertoire Sequences, Bulletin of Mathematical Biology, 85 (2023)

```
@article{bottcher2023mathematical,
  title={Mathematical Characterization of Private and Public Immune Receptor Sequences},
  author={B{\"o}ttcher, Lucas and Wald, Sascha and Chou, Tom},
  journal={Bulletin of Mathematical Biology},
  volume={85},
  number={10},
  pages={1--31},
  year={2023},
  publisher={Springer}
}
```
